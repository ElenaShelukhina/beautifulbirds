﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WorldOfBirds.Models;

namespace WorldOfBirds.Controllers
{
    public class AdminController : Controller
    {
        //
        // GET: /Admin/
        public ActionResult Index()
        {
            MessageRepository messageRepository = new MessageRepository();

            return View(messageRepository.GetMessages());
        }
	}
}