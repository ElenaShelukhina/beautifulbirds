﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WorldOfBirds.Models;

namespace WorldOfBirds.Controllers
{
    public class GalleryController : Controller
    {
        private readonly CategoryRepository _categoryRepository = new CategoryRepository();

        public ActionResult Gallery()
        {
            return View(_categoryRepository.GetBirdsCategories());
        }
	}
}