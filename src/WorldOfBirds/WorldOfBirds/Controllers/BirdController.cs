﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WorldOfBirds.Models;

namespace WorldOfBirds.Controllers
{
    public class BirdController: Controller
    {
        private readonly BirdRepository _birdRepository = new BirdRepository();

        public ActionResult Bird(int id)
        {
            var bird = _birdRepository.GetBirdById(id);
            return View(bird);
        }
    }
}