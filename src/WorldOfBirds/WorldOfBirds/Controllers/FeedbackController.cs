﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Mvc;
using WorldOfBirds.Models;



namespace WorldOfBirds.Controllers
{
    public class FeedbackController : Controller
    {
        private readonly MessageRepository _messageRepository = new MessageRepository();

        [HttpGet]
        public ViewResult Feedback()
        {            
            return View();
        }                

        [HttpPost] 
        public ActionResult Feedback(Message message)
        {
            if (ModelState.IsValid)
            {
                _messageRepository.SaveMessage(message);
                return View("Thanks", message);
            }
            else
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);              
            }
        }
    }
}
