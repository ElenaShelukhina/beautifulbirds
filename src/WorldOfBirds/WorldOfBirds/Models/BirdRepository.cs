﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;

namespace WorldOfBirds.Models

{ 
    public class BirdRepository 
    {
      private readonly BirdsDBEntities _db = new BirdsDBEntities();

        public BirdRepository()
        {
            
        }

        public IEnumerable<Bird>GetBirds()
        {
            return _db.Birds;
        }

        public Bird GetBirdById(int id)
         {
             return _db.Birds.FirstOrDefault(x => x.Id == id);
         }
    }
} 
