﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WorldOfBirds.Models
{
    public class MessageRepository
    {
        private readonly MessagesDBEntities _db = new MessagesDBEntities();

        public IEnumerable<Message> GetMessages()
        {
            return _db.Messages;
        }

        public Message GetMessageById(int id)
        {
            return _db.Messages.FirstOrDefault(x => x.Id == id);
        }

        public void SaveMessage(Message message)
        {
            message.Date = DateTime.Now;
            _db.Messages.Add(message);
            _db.SaveChanges();                        
        }
    }
}