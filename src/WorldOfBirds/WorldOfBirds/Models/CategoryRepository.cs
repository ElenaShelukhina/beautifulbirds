﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;

namespace WorldOfBirds.Models
{
    public class CategoryRepository
    {
        private readonly BirdsDBEntities _db=new BirdsDBEntities();

        public CategoryRepository()
        {
        }

        public IEnumerable<BirdsCategory> GetBirdsCategories()
        {
            return _db.BirdsCategories;
        }

        public BirdsCategory GetBirdsCategory(int id)
        {
            return _db.BirdsCategories.FirstOrDefault(x => x.Id == id);
        }
    }
}