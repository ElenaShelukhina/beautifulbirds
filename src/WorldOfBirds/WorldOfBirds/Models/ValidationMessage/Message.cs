﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace WorldOfBirds.Models
{
    [MetadataType(typeof(MessageValidation))]
    public partial class Message
    {        
    }

    public class MessageValidation
    {
        [Required(ErrorMessage = "Введите имя")]
        [StringLength(50)]
        public string Name { get; set; }

        [Required(ErrorMessage = "Введите Email адрес")]
        [RegularExpression(@".+\@.+\..+", ErrorMessage = "Введите допустимый Email адрес")]
        [StringLength(50)]
        public string Email { get; set; }

        [Required(ErrorMessage = "Введите текст сообщения")]
        [StringLength(4000)]
        public string Text { get; set; }
    }
}