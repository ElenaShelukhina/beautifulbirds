﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(WorldOfBirds.Startup))]
namespace WorldOfBirds
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
